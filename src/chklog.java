
import com.edi.common.Logging;
import com.edi.common.MsSqlConnection.MsSqlConnection;
import com.edi.common.MySqlConnection.MySqlConnection;
import com.edi.gmailer;
import java.awt.Label;
import java.sql.Statement;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * chklog.java
 * @author h.patel
 * Created on 12-Feb-2012, 19:13:34
 */

public class chklog extends javax.swing.JFrame {

    private Logging   log = new Logging("Automation");
    private String AppPath = System.getProperty("user.dir");
    private MySqlConnection mysql = null;
    private MsSqlConnection mssql = null;
    private Connection conn = null;
    private int bAutoRun = 0;
    private String ErrCode = "0";
    private String AppName = "";
    private String mailID = "h.patel@exchange-data.com";
    private String LogFilePath = AppPath + "\\";
    private String tbl = "";
    private String db = "";
    private String driver = "";
    private String DBConfigFile = "O:\\AUTO\\Configs\\DbServers.cfg";
    
    public chklog(String[] args) {
        
         // Set Look and Feel to Native style.
        try {
	    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e){
            //System.out.println(e);
	}
        initComponents();
        try{

            if(args.length<=0){
                System.out.println("No Parameter found");
                exitForm(Integer.parseInt(ErrCode));
            }

            for(int f=0;f<args.length;f++){
                if(args[f].equalsIgnoreCase("-m")){
                    bAutoRun = 1;
                }else if(args[f].equalsIgnoreCase("-a")){
                   // bt_click.setEnabled(false);
                    bAutoRun = 2;
                }else if(args[f].equalsIgnoreCase("-l")){
                    LogFilePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-sf")){
                    DBConfigFile = args[f+1];
                }else if(args[f].equalsIgnoreCase("-mailID")){
                    mailID = args[f+1];
                }
            }

            setVisible(true);

            if (bAutoRun==2){
                bt_clickActionPerformed(null);
                exitForm(0);
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"Exe Parameter Failed: " + e,"Invalid Exe Paramenter",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("Exe Parameter Failed: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
        
    }

    private void exitForm(int code) {
       if(code == 0)
            dispose();
       else
           System.exit(1);
    }

    private void GetConn(String svr){
        try{
            String [] connDetail = GetConnDetail(svr);            
            if(connDetail[0].trim().compareToIgnoreCase("mysql")==0){
                mysql = new MySqlConnection(connDetail[1],connDetail[2],db,connDetail[3]);
                conn = mysql.getConnection();                
            }else if(connDetail[0].trim().compareToIgnoreCase("mssql")==0){
                mssql = new MsSqlConnection(connDetail[1],connDetail[2],db,connDetail[3]);
                conn = mssql.getConnection();
            }
       }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"OpenDBConn: " + e.getMessage());
            }else{
                log.append("OpenDBConn: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
       }
    }

    private String [] GetConnDetail(String svr){
      try{
          String [] detail = new String[4];
          String [] tvalues = null;
           File sourcefile = new File(DBConfigFile);
            if(sourcefile.exists()){
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    tvalues = str.split("\t");
                    if(tvalues[0].compareToIgnoreCase(svr)==0){
                        detail[0] = tvalues[1];
                        detail[1] = tvalues[2];
                        detail[2] = tvalues[3];
                        detail[3] = tvalues[4];
                    }
                }
                in.close();
            } else{
                log.append("Could not found the config file: O:\\AUTO\\Configs\\DbServers.cfg",  LogFilePath, "ChkWebAppLog");
            }

          return detail;
      }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
            return null;
       }
  }

 public void Closeconn(){
        conn = null;
 }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new javax.swing.JPanel();
        bt_click = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        atxt = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Check Web Application's Logs");

        bt_click.setText("Click");
        bt_click.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_clickActionPerformed(evt);
            }
        });

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        atxt.setColumns(20);
        atxt.setRows(5);
        atxt.setEnabled(false);
        jScrollPane1.setViewportView(atxt);

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(bt_click)
                .addContainerGap())
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                    .addComponent(bt_click, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt_clickActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_clickActionPerformed
        try{
            bt_click.setEnabled(false);
            //Check on DOTCOM
            chkWCALoaderLog("COM_MY_EDI"/*,"COM_MS_AHD1099"*/);
            chkSMFLoaderLog("COM_MY_EDI"/*,"COM_MS_AHD1099"*/);
            chkFeedByCountryLog("COM_MY_EDI");

            //Check on DOTNET
            chkWCALoaderLog("iCloud_MySql"/*,"NET_MS_Exchange2"*/);
            chkSMFLoaderLog("iCloud_MySql"/*,"NET_MS_Exchange2"*/);
            chkFeedByCountryLog("iCloud_MySql");
            atxt.setText("Finished Checking");
            Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
            bt_click.setEnabled(true);
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"bt_clickActionPerformed: " + e.getMessage());
            }else{
                log.append("bt_clickActionPerformed: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
        }
    }//GEN-LAST:event_bt_clickActionPerformed

    private void chkWCALoaderLog(String  mysvr/*, String mssvr*/){
        try{
                int z1 = 0, z2 = 0, z3 = 0;
                String dt = "";
                //mysql check
                atxt.setText("Checking for WCA Loader\nFor MYSQL server " + mysvr + "\nPlease wait...... " );
                Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                GetConn(mysvr);
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select file_name, replace(curdate(),'-','') as dt from wca.tbl_opslog " +
                                                 " where SUBSTRING_INDEX(file_name, '.', 1) = replace(curdate(),'-','') " +
                                                 "  and mode like 'Daily'");


                if(rs != null){
                    while(rs.next()){
                        if(rs.getString("file_name").trim().toLowerCase().endsWith(".z01")){
                            //System.out.println(rs.getString("file_name"));
                            z1++;
                        }else if(rs.getString("file_name").trim().toLowerCase().endsWith(".z02")){
                           // System.out.println(rs.getString("file_name"));
                            z2++;
                        }else if(rs.getString("file_name").trim().toLowerCase().endsWith(".z03")){
                           // System.out.println(rs.getString("file_name"));
                            z3++;
                        }
                        dt =rs.getString("dt");
                    }
                }
                rs.close();
                if(dt.isEmpty()){
                    rs = stmt.executeQuery("select replace(curdate(),'-','') as dt ");
                    if(rs != null){
                        rs.next();
                        dt = rs.getString("dt");
                    }
                }
                if(z1 != 1){
                    //System.out.println("Please Check");
                    SendEmailAlert(mailID, "Check WCA", "Please check " + dt + ".Z01 is not loaded on the server:" + mysvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\WCALoader.doc");
                }

                if(z2 != 1){
                    //System.out.println("Please Check");
                    SendEmailAlert(mailID, "Check WCA", "Please check " + dt + ".Z02 is not loaded on the server:" + mysvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\WCALoader.doc");
                }

                if(z3 != 1){
                    //System.out.println("Please Check");
                    SendEmailAlert(mailID, "Check WCA", "Please check " + dt + ".Z03 is not loaded on the server:" + mysvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\WCALoader.doc");
                }
                rs.close();
                stmt.close();
                Closeconn();

              /*  //mssql check
                z1 = 0; z2 = 0; z3 = 0;
                dt = "";
                atxt.setText("Checking for WCA Loader\nFor MSSQL server " + mssvr + "\nPlease wait...... ");
                Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                GetConn(mssvr);
                stmt = conn.createStatement();
                rs = stmt.executeQuery("USE wca " +
                                       "select file_name, convert(varchar(8),getdate(),112) as dt from tbl_opslog " +
                                       " where Left(file_name,8) = convert(varchar(8),getdate(),112) " +
                                       " and mode like 'Daily'");

                if(rs != null){
                    while(rs.next()){
                        if(rs.getString("file_name").trim().toLowerCase().endsWith(".z01")){
                            //System.out.println(rs.getString("file_name"));
                            z1++;
                        }else if(rs.getString("file_name").trim().toLowerCase().endsWith(".z02")){
                            //System.out.println(rs.getString("file_name"));
                            z2++;
                        }else if(rs.getString("file_name").trim().toLowerCase().endsWith(".z03")){
                            //System.out.println(rs.getString("file_name"));
                            z3++;
                        }
                        dt =rs.getString("dt");
                    }
                }

                rs.close();
                if(dt.isEmpty()){
                    rs = stmt.executeQuery("select convert(varchar(8),getdate(),112) as dt ");
                    if(rs != null){
                        rs.next();
                        dt = rs.getString("dt");
                    }
                }
                if(z1 != 1){
                    //System.out.println("Please Check");
                    SendEmailAlert(mailID, "Check WCA", "Please check " + dt + ".Z01 is not loaded on the server:" + mssvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\WCALoader.doc");
                }

                if(z2 != 1){
                    //System.out.println("Please Check");
                    SendEmailAlert(mailID, "Check WCA", "Please check " + dt + ".Z02 is not loaded on the server:" + mssvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\WCALoader.doc");
                }

                if(z3 != 1){
                    //System.out.println("Please Check");
                    SendEmailAlert(mailID, "Check WCA", "Please check " + dt + ".Z03 is not loaded on the server:" + mssvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\WCALoader.doc");
                }
                rs.close();
                stmt.close();
                Closeconn();        */
        }catch(Exception e){
           if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
        }
    }   

    private void chkSMFLoaderLog(String  mysvr/*, String mssvr*/){
        try{
                
                //mysql check
                atxt.setText("Checking for SMF Loader\nFor MYSQL server " + mysvr + "\nPlease wait...... ");
                Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                GetConn(mysvr);
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select count(*) as cnt, concat(date_sub(curdate(), interval 1 day), ' 18:00:00') as dt FROM smf4.tbl_opslog " +
                                                 " where feeddate = concat(date_sub(curdate(), interval 1 day), ' 18:00:00') "+
                                                 " and seccnt > 0");


                if(rs != null){
                   rs.next();
                   if(rs.getInt("cnt") == 0){
                       //System.out.println("Please Check");
                       SendEmailAlert(mailID, "Check SMF", "Please Check " + rs.getString("dt") + " feed is loaded on the server:" + mysvr +
                                           "\n Document is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\SMFLoader.doc");
                   }
                }
                rs.close();
                stmt.close();
                Closeconn();

               /* //mssql check
                atxt.setText("Checking for SMF Loader\nFor MSSQL server " + mssvr + "\nPlease wait...... ");
                Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                GetConn(mssvr);
                stmt = conn.createStatement();
                rs = stmt.executeQuery("use smf4 " +
                                       " select count(*) as cnt, convert(varchar(10),getdate()-1,120) + ' 18:00:00' as dt FROM tbl_opslog " +
                                       " where feeddate = convert(varchar(8),getdate()-1,112) + ' 18:00:00' " +
                                       " and seccnt > 0");

                if(rs != null){
                   rs.next();
                   if(rs.getInt("cnt") == 0){
                       //System.out.println("Please Check");
                       SendEmailAlert(mailID, "Check SMF", "Please Check " + rs.getString("dt") + " feed is loaded on the server:" + mssvr +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\SMFLoader.doc");
                   }
                }                 
                rs.close();
                stmt.close();
                Closeconn();*/
        }catch(Exception e){
           if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
        }
    }

    private void chkFeedByCountryLog(String  mysvr){
        try{                
                //mysql check
                atxt.setText("Checking for FeedByCountry\nFor MYSQL server " + mysvr + "\nPlease wait...... ");
                Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                GetConn(mysvr);
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select * from webapp.fbc " +
                                                 " where file_lastoutput < curdate()");


                if(rs != null){
                   while(rs.next()){
                        //System.out.println("Please Check " + rs.getString("source_folder"));
                        SendEmailAlert(mailID, "Check FeedByCountry", "Please Check fbc progam on the server:" + mysvr + " for " + rs.getString("source_folder") +
                                           "\nDocument is available in folder \\\\192.168.12.4\\it_info\\JAVA\\Himadri\\FeedByCountry.doc");
                   }
                }
                rs.close();
                stmt.close();
                Closeconn();

        }catch(Exception e){
           if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
        }
    }


    private void SendEmailAlert(String strTO, String sub, String strMessage){
        try{
            gmailer alert = new gmailer();
            alert.Init("ops@exchange-data.com", "BLOCH1");
            alert.setRecipients(strTO);
            alert.setSubject(sub);
            alert.setMessageText(strMessage);
            alert.sendMessage();
        }catch (Exception e) {
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"SendEmailAlert:" + e.getMessage());
            }else{
                log.append("SendEmailAlert:" + e.getMessage(), LogFilePath, "WCALoader");
            }
        }
}

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        new chklog(args);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JTextArea atxt;
    private javax.swing.JButton bt_click;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

}
